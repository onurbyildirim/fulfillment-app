import React, { Component } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import Login from './Login';
import Register from './Register';
import fourOhFour from './fourOhFour';
import DashboardLayout from './protected/DashboardLayout.js';
import { Hub, Logger } from 'aws-amplify';
import { currentAuthenticatedUserIfAny } from '../helpers/auth';

function PrivateRoute({ component: Component, authed, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        authed === true ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: '/login', state: { from: props.location } }}
          />
        )
      }
    />
  );
}

class Nav extends Component {
  constructor() {
    super();
    this.state = {
      authed: false,
      loading: false,
      user: {}
    };
  }

  componentDidMount() {
    this.authListener();
    this.checkUser();
  }
  componentWillUnmount() {}
  authListener = () => {
    const logger = new Logger('Auth Listener');
    logger.onHubCapsule = capsule => {
      const { channel, payload } = capsule;
      // console.log('Channel: ', channel);
      // console.log('Payload: ', payload);

      if (channel === 'auth') {
        // eslint-disable-next-line default-case
        switch (payload.event) {
          case 'signIn':
            const { user } = payload.data.signInUserSession.idToken.payload;
            this.setState({ authed: true, loading: false, user: user });
            this.props.history.push('/dashboard');
            break;
          case 'signUp':
            this.props.history.push('/login');
            break;
          case 'signOut':
            this.setState({ authed: false, loading: false, user: {} });
            this.props.history.push('/');
        }
      }
    };
    Hub.listen('auth', logger);
  };
  checkUser = () => {
    currentAuthenticatedUserIfAny()
      .then(user => {
        console.log(user);
        this.setState({ authed: true, loading: false, user: user });
        this.props.history.push('/dashboard');
      })
      .catch(err => {
        console.log(err);
        this.setState({ authed: false, user: {} });
      });
  };
  render() {
    return this.state.loading === true ? (
      <h1>Loading</h1>
    ) : (
      <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/login" exact component={Login} />
        <Route path="/register" component={Register} />
        <PrivateRoute
          authed={this.state.authed}
          path="/dashboard"
          component={DashboardLayout}
        />
        {/* <Route path="/dashboard" exact component={Dashboard} /> */}

        <Route component={fourOhFour} />
      </Switch>
    );
  }
}

export default withRouter(Nav);
