import React, { Component } from 'react';
import { register } from '../helpers/auth';

function setErrorMsg(error) {
  return {
    registerError: error.message
  };
}

export default class Register extends Component {
  state = { registerError: null };
  handleSubmit = e => {
    e.preventDefault();
    console.log(this.pwReType.value);
    console.log(this.pw.value);

    if (this.pw.value === this.pwReType.value) {
      register(this.email.value, this.pw.value)
        .then(data => console.log(data))
        .catch(e => this.setState(setErrorMsg(e)));
    } else {
      this.setState(setErrorMsg({ message: 'Passwords have to match.' }));
    }
  };
  goToLogin = () => {
    this.props.history.push('/');
  };

  render() {
    return (
      <div className="app flex-row align-items-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8">
              <form onSubmit={this.handleSubmit}>
                <div className="card p-4">
                  <div className="class-body">
                    <h1 className="mb-3">Register</h1>
                    <div className="input-group mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="fas fa-user" />
                        </span>
                      </div>
                      <input
                        className="form-control"
                        type="email"
                        ref={email => (this.email = email)}
                        placeholder="Email"
                      />
                    </div>
                    <div className="input-group mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="fas fa-unlock" />
                        </span>
                      </div>
                      <input
                        className="form-control"
                        type="password"
                        ref={pw => (this.pw = pw)}
                        placeholder="Password"
                      />
                    </div>
                    <div className="input-group mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="fas fa-unlock" />
                        </span>
                      </div>
                      <input
                        className="form-control"
                        type="password"
                        placeholder="Re-Type"
                        ref={pwReType => (this.pwReType = pwReType)}
                      />
                    </div>
                    {this.state.loginMessage && (
                      <div className="row mb-3">
                        <div className="col-6">
                          <p className="text-danger">
                            Error: {this.state.loginMessage}{' '}
                          </p>
                        </div>
                        <div className="col-6 text-right">
                          <button className="btn btn-link p-0" type="button">
                            Forgot password?
                          </button>
                        </div>
                      </div>
                    )}
                    <div className="row">
                      <div className="col-6">
                        <button
                          className="btn btn-primary px-4 btn-lg w-100"
                          type="submit"
                        >
                          Register
                        </button>
                      </div>
                      <div className="col-6">
                        <button
                          className="btn btn-light px-4 btn-lg w-100"
                          type="button"
                          onClick={this.goToLogin}
                        >
                          Back to Login
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
  // render() {
  //   return (
  //     <div className="section loginContainer">
  //       <div className="container" style={{ width: '40vh' }}>
  //         <h1 className="title">Register</h1>
  //         <form onSubmit={this.handleSubmit}>
  //           <div className="field">
  //             <div className="control has-icons-left has-icons-right">
  //               <input
  //                 className="input"
  //                 ref={email => (this.email = email)}
  //                 placeholder="Email"
  //               />
  //               <span className="icon is-small is-left">
  //                 <i className="fa fa-envelope" />
  //               </span>
  //             </div>
  //           </div>
  //           <div className="field">
  //             <p className="control has-icons-left">
  //               <input
  //                 className="input"
  //                 type="password"
  //                 placeholder="Password"
  //                 ref={pw => (this.pw = pw)}
  //               />
  //               <span className="icon is-small is-left">
  //                 <i className="fa fa-lock" />
  //               </span>
  //             </p>
  //           </div>
  //           <div className="field">
  //             <p className="control has-icons-left">
  //               <input
  //                 className="input"
  //                 type="password"
  //                 placeholder="Re-Type"
  //                 ref={pwReType => (this.pwReType = pwReType)}
  //               />
  //               <span className="icon is-small is-left">
  //                 <i className="fa fa-lock" />
  //               </span>
  //             </p>
  //           </div>
  //           {this.state.registerError && (
  //             <div className="alert alert-danger" role="alert">
  //               <span
  //                 className="glyphicon glyphicon-exclamation-sign"
  //                 aria-hidden="true"
  //               />
  //               <span className="sr-only">Error:</span>
  //               &nbsp;{this.state.registerError}
  //             </div>
  //           )}
  //           <div className="field is-grouped">
  //             <div className="control">
  //               <button type="submit" className="button is-primary">
  //                 Register
  //               </button>
  //             </div>
  //             <div className="control">
  //               <button className="button is-link" onClick={this.goToLogin}>
  //                 Back to Login
  //               </button>
  //             </div>
  //           </div>
  //         </form>
  //       </div>
  //     </div>
  //   );
  // }
}
