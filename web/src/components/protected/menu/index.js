import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';

export default function Menu({ match }) {
  return (
    <Fragment>
      <nav className="sidebar-nav">
        <ul className="nav">
          <li className="nav-item">
            <Link className="nav-link" to={`${match.url}/orders`}>
              Orders
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to={`${match.url}/scans`}>
              Scans
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to={`${match.url}/users`}>
              Users
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to={`${match.url}/reports`}>
              Reports
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to={`${match.url}/qvc`}>
              QVC Reports
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to={`${match.url}/tickets`}>
              Tickets
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to={`${match.url}/promo`}>
              Promo Codes
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to={`${match.url}/products`}>
              Products
            </Link>
          </li>
        </ul>
      </nav>
    </Fragment>
  );
}
