import React, { Component, Fragment } from 'react';
import { login, currentAuthenticatedUserIfAny } from '../helpers/auth';

function setErrorMsg(error) {
  return {
    loginMessage: error.message
  };
}

export default class Login extends Component {
  constructor() {
    super();
    this.state = { loginMessage: null };
  }

  handleSubmit = e => {
    e.preventDefault();
    login(this.email.value, this.pw.value)
      .then(user => console.log('User', user))
      .catch(e => this.setState(setErrorMsg(e)));
  };

  resetPassword = () => {};
  checkAuth = () => {
    currentAuthenticatedUserIfAny()
      .then(user => console.log(user))
      .catch(err => console.log('err', err));
  };

  render() {
    return (
      <Fragment>
        <div className="app flex-row align-items-center">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-8">
                <form onSubmit={this.handleSubmit}>
                  <div className="card p-4">
                    <div className="class-body">
                      <h1 className="mb-3">Login</h1>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="fas fa-user" />
                          </span>
                        </div>
                        <input
                          className="form-control"
                          type="email"
                          ref={email => (this.email = email)}
                          placeholder="Email"
                        />
                      </div>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="fas fa-unlock" />
                          </span>
                        </div>
                        <input
                          className="form-control"
                          type="password"
                          ref={pw => (this.pw = pw)}
                          placeholder="Password"
                        />
                      </div>
                      {this.state.loginMessage && (
                        <div className="row mb-3">
                          <div className="col-6">
                            <p className="text-danger">
                              Error: {this.state.loginMessage}{' '}
                            </p>
                          </div>
                          <div className="col-6 text-right">
                            <button className="btn btn-link p-0" type="button">
                              Forgot password?
                            </button>
                          </div>
                        </div>
                      )}
                      <div className="row">
                        <div className="col-12">
                          <button
                            className="btn btn-primary px-4 btn-lg w-100"
                            type="submit"
                          >
                            Login
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
