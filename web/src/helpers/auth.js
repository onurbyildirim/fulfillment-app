import Amplify, { Auth } from 'aws-amplify';
import config from '../config/aws-exports';
Amplify.configure(config);

export async function login(email, pass) {
  console.log(email, pass);
  return await Auth.signIn(email, pass);
}

export async function register(email, pass) {
  return await Auth.signUp({
    username: email,
    password: pass,
    attributes: { email }
  });
}

export async function currentAuthenticatedUserIfAny() {
  return await Auth.currentAuthenticatedUser();
}

export async function signOut() {
  return await Auth.signOut()
    .then(data => console.log(data))
    .catch(err => console.log(err));
}
