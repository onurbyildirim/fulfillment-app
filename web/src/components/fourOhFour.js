import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class fourOhFour extends Component {
  render() {
    return (
      <div className="app flex-row align-items-center text-center">
        <div className="container">
          <h3>Ayooo!! You got lost,let's get u back...</h3>
          <h5>
            <Link to="/dashboard">Homepage</Link>
          </h5>
        </div>
      </div>
    );
  }
}
