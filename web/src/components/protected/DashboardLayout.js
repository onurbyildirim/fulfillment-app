import React, { Component } from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import Menu from './menu';
import Dashboard from './dashboard';
import Orders from './orders';
import { signOut } from '../../helpers/auth';
class DashboardLayout extends Component {
  render() {
    let { match, location } = this.props;
    let subtitle = location.pathname.split('/')[2];
    console.log(match);
    return (
      <div className="app sidebar-show">
        <header className="app-header navbar navbar-expand-lg navbar-light bg-light">
          <Link className="navbar-brand" to="/dashboard">
            <h1 style={{ fontSize: '1em' }}>bareMinerals</h1>
          </Link>
          <div className="collapse navbar-collapse float-right">
            <button
              className="btn btn-outline-danger my-2 my-sm-0"
              onClick={() => signOut()}
            >
              Sign Out
            </button>
          </div>
        </header>
        <div className="app-body">
          <div className="sidebar">{<Menu match={match} />}</div>
          <main className="main">
            <div className="container-fluid">
              <Route exact path={`${match.url}`} render={() => <Dashboard />} />
              <Route
                path={`/dashboard/orders`}
                render={() => <Orders {...this.props} />}
              />
              <Route
                path={`${match.url}/scans`}
                render={() => <div>Home</div>}
              />
            </div>
          </main>
        </div>
      </div>
    );
  }
}

export default DashboardLayout;
